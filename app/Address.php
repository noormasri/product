<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
        public $timestamps = false;

    protected $table = 'addresses';
    protected $fillable = [
        'city'];

        public function Product(){

        return $this->hasMany('App\Product');
    }
}


//<?php
//
//namespace App;
//
//use Illuminate\Database\Eloquent\Model;
//
//class Address extends Model
//{
//    public $timestamps = false;
//
//    protected $table = 'address';
//
//    public function Product(){
//
//        return $this->hasMany('App\Product');
//    }
//}
