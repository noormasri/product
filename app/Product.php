<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{

        public $timestamps = false;
  protected $table = 'products';

    protected $fillable = [
        'title',
        'description',
        'photo',
        'city_id'
    ];

  public  function  Address(){
        return $this->belongsTo('App\Product');

   }
}

