<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Input;
use App\Product as Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index()
    {

        $product = Product::all();

        return response()->json($product);


    }

    public function create(Product $product)
    {
        return response()->json($product);
    }


    public function store(Request $request)
    {

        $request->validate([
            'title' => 'required',
            'city_id' => 'required',

            'description' => 'required',

            'photo' => 'required',


        ]);
//        $product_data = array(
//            'title' => Input::get('title'),
//            'description' => Input::get('description'),
//            'photo' => '',
//            'city_id' => Input::get('city_id'),
//        );
//        if (Input::hasFile('photo')) {
//            $destinationPath = 'uploads';
//            $photo = Input::file('photo');
//            $photo_name = $photo->getClientOriginalName();
//            $photo->move($destinationPath,$photo_name);
//            $product_data['photo'] = $photo_name;
//        }
//        $product_id = Product::insert($product_data);
        $product = Product::create($request->all());

        return response()->json([
            'success' => 'Product has been added',
            'product' => $product            ]);

    }




    public function destroy(Product $product)
    {
        $product->delete();

        return response()->json([
            'success' => 'Product has been deleted Successfully'
        ]);



    }


    public function edit(Product $product)
    {
        return response()->json($product);


    }



    public function update(Request $request,Product $product)
    {

        $request->validate([
            'title' => 'required',
            'city_id' => 'required',

            'description' => 'required',

            'photo' => 'required',
        ]);


//        if (Input::hasFile('photo')) {
//            $destinationPath = 'uploads';
//            $photo = Input::file('photo');
//            $photo_name = $photo->getClientOriginalName();
//            $photo->move($destinationPath,$photo_name);
//            @unlink($destinationPath.'/'.$product->photo);
//        }
//        else
//        {
//            $photo_name=$product->photo;
//        }
//
//        $product_data = array(
//            'title' => Input::get('title'),
//            'description' => Input::get('description'),
//            'photo' => $photo_name,
//            'city_id' => Input::get('city_id'),
//        );
//        $product_id = Product::where('id', '=', $id)->update($product_data);

        $product->update($request->all());

        return response()->json([
            'success' => 'Product has been updated',
            'product' => $product
        ]);
    }
    public function show(Product $product)
    {
        return response()->json($product,200);

    }
}
