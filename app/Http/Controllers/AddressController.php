<?php namespace App\Http\Controllers;

use App\Address as Address;
use Illuminate\Http\Request;

use Hash;

class AddressController extends Controller
{

    public function index()
        {

            $address = Address::all();
            return response()->json($address);

        }

    public function create(Address $address)
        {
            return response()->json($address);
        }

    public function store(Request $request)


        {


            $request->validate([
                'city' => 'required',

            ]);

            $address = Address::create($request->get('city'));

            return response()->json([
                'success' => 'Address has been added',
                'address' => $address
            ]);



        }


    public function destroy(Address $address)
        {

            //return response()->json($address->delete(),204);

            $address->delete();

            return response()->json([
                'success' => 'address has been deleted Successfully'
            ]);
//            $address = Address::find($id);
//            $address->delete();
//
//            return redirect('/address')->with('success', 'address has been deleted Successfully');
        }

    public function edit(Address $address)
        {

            return response($address);


//            $address = Address::find($id);
//
//            return view('address.edit', compact('address'));
        }



    public function update(Request $request,Address $address)
        {
            $request->validate([
                'city' => 'required',
            ]);

            $address->update($request->all());

            return response()->json([
                'success' => 'Address has been updated',
                'address' => $address
            ]);



        }
    public function show(Address $address)
    {
        //
        return response()->json($address,200);


    }

}