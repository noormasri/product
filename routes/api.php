<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});



Route::get('/address', 'AddressController@index')->name('address.index');
Route::post('/address', 'AddressController@store')->name('address.store');
Route::post('/address/{address}', 'AddressController@edit')->name('product.edit');
Route::get('/address/{address}', 'AddressController@show')->name('address.show');
Route::put('/address/{address}', 'AddressController@update')->name('address.update');
Route::delete('/address/{address}', 'AddressController@destory')->name('address.destroy');
Route::post('address/create', 'AddressController@create');




Route::get('/product', 'ProductController@index')->name('product.index');
Route::post('/product', 'ProductController@store')->name('product.store');
Route::post('/product/{address}', 'ProductController@edit')->name('product.edit');
Route::get('/product/{product}', 'ProductController@show')->name('product.show');
Route::put('/product/{product}', 'ProductController@update')->name('product.update');
Route::delete('/product/{product}', 'ProductController@destory')->name('product.destroy');
Route::post('product/create', 'ProductController@create');


Route::post('login', 'APILoginController@login');
Route::middleware('jwt.auth')->get('users', function () {
    return auth('api')->user();
});