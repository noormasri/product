



@extends('layouts')

@section('content')
    <style>
        .uper {
            margin-top: 40px;
        }
    </style>
    <div class="card uper">
        <div class="card-header">
            Edit Product
        </div>
        <div class="card-body">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div><br />
            @endif

            {{--<nav class="navbar navbar-inverse">--}}
            {{--<div class="container-fluid">--}}
            {{--<div class="navbar-header">--}}
            {{--<a class="navbar-brand" href="http://crudegenerator.in">Laravel Crud By Crud Generator</a>--}}
            {{--</div>--}}
            {{--<ul class="nav navbar-nav">--}}
            {{--<li><a href="{{route('address')}}">Manage Address</a></li>--}}
            {{--<li><a href="{{route('address.add')}}">Add Address</a></li>--}}
            {{--</ul>--}}
            {{--</div>--}}
            {{--</nav>--}}


                <div class="container">

                    <h2>{{ __('product.update_product')  }}</h2>
                    <form method="post" action="{{ route('product.update', $product->id) }}">
                        @method('PATCH')
                        @csrf
                        <input type="hidden" value="<?php echo $product->id ?>"   name="product_id">

                        <div class="form-group">
                            <label for="title">{{ __('product.title')  }}:</label>
                            <input type="text" value="<?php echo $product->title ?>" class="form-control" id="title" name="title">
                        </div>

                        <div class="form-group">
                            <label for="description">{{ __('product.description')  }}:</label>
                            <input type="text" value="<?php echo $product->description ?>" class="form-control" id="description" name="description">
                        </div>

                        <div class="form-group">
                            <label for="photo">{{ __('product.photo')  }}:</label>
                            <input type="file"  class="btn btn-primary" id="photo" name="photo">
                        </div>

                        <div class="form-group">
                            <label for="city_id">{{ __('product.city_id')  }}:</label>
                            <input type="number" value="<?php echo $product->city_id ?>" class="form-control" id="city_id" name="city_id">
                        </div>


                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
@endsection