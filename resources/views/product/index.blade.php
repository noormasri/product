

@extends('layouts')

@section('content')
    <style>
        .uper {
            margin-top: 40px;
        }
    </style>
    <div class="uper">
        @if(session()->get('success'))
            <div class="alert alert-success">
                {{ session()->get('success') }}
            </div><br />
        @endif

            {{--<nav class="navbar navbar-inverse">--}}
                {{--<div class="container-fluid">--}}
                    {{--<div class="navbar-header">--}}
                        {{--<a class="navbar-brand" href="http://crudegenerator.in">Laravel Crud By Noor</a>--}}
                    {{--</div>--}}
                    {{--<ul class="nav navbar-nav">--}}
                        {{--<li class="active" ><a href="{{route('product')}}">{{ __('product.manage_product')  }}</a></li>--}}
                        {{--<li><a href="{{route('product.add')}}">{{ __('product.add-product')  }}</a></li>--}}
                    {{--</ul>--}}
                {{--</div>--}}
            {{--</nav>--}}


            <div class="container">

                <h2>{{ __('product.manage_product')  }}</h2>



                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>{{ __('product.id')  }}</th>

                            <th>{{ __('product.title')  }}</th>
                            <th>{{ __('product.description')  }}</th>
                            <th>{{ __('product.photo')  }}</th>
                            <th>{{ __('product.city_id')  }}</th>
                            <th>{{ __('product.actions')  }}</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($product as $product)
                            <tr>
                                <td>{{$product->id}} </td>
                                <td>{{$product->title}} </td>
                                <td>{{$product->description}} </td>
                                <td> <img src="uploads/{{$product->photo}}" > </td>
                                <td>{{$product->city_id}} </td>



                                <td><a href="{{ route('product.edit',$product->id)}}" class="btn btn-primary">Edit</a>

                                    <form action="{{ route('product.destroy', $product->id)}}" method="post">
                                        @csrf
                                        @method('DELETE')
                                        <button class="btn btn-danger" type="submit">Delete</button>
                                    </form>
                                </td>


                            </tr>

                        @endforeach
                        </tbody>
                    </table>

            </div>

@endsection
