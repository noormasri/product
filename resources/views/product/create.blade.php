



@extends('layouts')

@section('content')
    <style>
        .uper {
            margin-top: 40px;
        }
    </style>
    <div class="card uper">
        <div class="card-header">
            Add Product
        </div>
        <div class="card-body">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div><br />
            @endif
                <div class="container">

                    <h2>{{ __('product.add_product')  }}</h2>
                    <form role="form" method="post"action="{{route('product.store')}} "   enctype="multipart/form-data" >
                        @method('PATCH')
                        @csrf
                        <div class="form-group">
                            <label for="title">{{ __('product.title')  }}:</label>
                            <input type="text" class="form-control" id="title" name="title">
                        </div>

                        <div class="form-group">
                            <label for="description">{{ __('product.description')  }}:</label>
                            <input type="text" class="form-control" id="description" name="description">
                        </div>

                        <div class="form-group">
                            <label for="photo">{{ __('product.photo')  }}:</label>
                            <input type="file" class="btn btn-primary" id="photo" name="photo">
                        </div>


                        <div class="form-group">
                            <label for="city_id">{{ __('product.city_id')  }}:</label>
                            <input type="number" class="form-control" id="city_id" name="city_id">
                        </div>


                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
@endsection