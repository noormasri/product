


                @extends('layouts')

                @section('content')
                    <style>
                        .uper {
                            margin-top: 40px;
                        }
                    </style>
                    <div class="card uper">
                        <div class="card-header">
                            Edit Product
                        </div>
                        <div class="card-body">
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div><br />
                            @endif

                                {{--<nav class="navbar navbar-inverse">--}}
                                {{--<div class="container-fluid">--}}
                                {{--<div class="navbar-header">--}}
                                {{--<a class="navbar-brand" href="http://crudegenerator.in">Laravel Crud By Crud Generator</a>--}}
                                {{--</div>--}}
                                {{--<ul class="nav navbar-nav">--}}
                                {{--<li><a href="{{route('address')}}">Manage Address</a></li>--}}
                                {{--<li><a href="{{route('address.add')}}">Add Address</a></li>--}}
                                {{--</ul>--}}
                                {{--</div>--}}
                                {{--</nav>--}}

                                <form method="post" action="{{ route('address.update', $address->id) }}">
                                @method('PATCH')
                                @csrf
                                <div class="form-group">
                                    <label for="city">City:</label>
                                    <input type="text" class="form-control" name="city" value={{ $address->city }} />
                                </div>

                                <button type="submit" class="btn btn-primary">Update</button>
                            </form>
                        </div>
                    </div>
@endsection