@extends('layouts')

@section('content')
    <style>
        .uper {
            margin-top: 40px;
        }
    </style>
    <div class="uper">
        @if(session()->get('success'))
            <div class="alert alert-success">
                {{ session()->get('success') }}
            </div><br />
        @endif
{{--<nav class="navbar navbar-inverse">--}}
    {{--<div class="container-fluid">--}}
        {{--<div class="navbar-header">--}}
            {{--<a class="navbar-brand" href="http://crudegenerator.in">Laravel Crud By Crud Generator</a>--}}
        {{--</div>--}}
        {{--<ul class="nav navbar-nav">--}}
            {{--<li class="active" ><a href="{{route('address')}}">Manage Address</a></li>--}}
            {{--<li><a href="{{route('address.add')}}">Add Address</a></li>--}}
        {{--</ul>--}}
    {{--</div>--}}
{{--</nav>--}}


    <h2>{{ __('address.manage_address')  }}</h2>



            <table class="table table-striped">
                <thead>
                <tr>
                    <th>{{ __('address.id')  }}</th>
                    <th>{{ __('address.city')  }}</th>
                    <th>{{ __('address.actions')  }}</th>
                </tr>
                </thead>
                <tbody>
                @foreach($address as $address)
                    <tr>
                        <td>{{$address->id}}</td>
                        <td>{{$address->city}}</td>

                        <td><a href="{{ route('address.edit',$address->id)}}" class="btn btn-primary">Edit</a></td>
                        <td>
                            <form action="{{ route('address.destroy', $address->id)}}" method="post">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-danger" type="submit">Delete</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div>
@endsection
