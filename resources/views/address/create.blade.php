


                @extends('layouts')

                @section('content')
                    <style>
                        .uper {
                            margin-top: 40px;
                        }
                    </style>
                    <div class="card uper">
                        <div class="card-header">
                            Add Product
                        </div>
                        <div class="card-body">
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div><br />
                            @endif
                            <form method="post" action="{{ route('address.store') }}">
                                <div class="form-group">
                                    @csrf

                                <div class="form-group">
                                    <label for="city">City:</label>
                                    <input type="text" class="form-control" name="city"/>
                                </div>
                                <button type="submit" class="btn btn-primary">Add</button>
                                </div>
                            </form>


                    </div>
@endsection